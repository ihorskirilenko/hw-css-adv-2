'use strict'

function setBgSize() {
    let bg = document.querySelector('.banner');
    let bgWidth = window.getComputedStyle(bg).getPropertyValue('width');
    bgWidth = Math.round(bgWidth.slice(0, bgWidth.length - 2));
    bg.style.height = `${bgWidth * 0.5}px`
}

window.addEventListener('resize', setBgSize);
window.addEventListener('load', setBgSize);


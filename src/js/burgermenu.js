'use strict'

document.querySelectorAll('.top-nav i').forEach(el => {
    el.addEventListener('click', function() {
        document.querySelector('.top-nav__list').classList.toggle('active');
        document.querySelectorAll('.top-nav__item').forEach(el => {
            el.classList.toggle('active');
        })
        document.querySelectorAll('.top-nav i').forEach(el => {
            el.classList.toggle('active');
        })
    })
})


window.addEventListener('resize', () => {
    document.querySelector('.top-nav__list').classList.remove('active');
    document.querySelectorAll('.top-nav *').forEach(el => {
        el.classList.remove('active');
    })
    document.querySelector('.top-nav i:first-child').classList.add('active');
})
